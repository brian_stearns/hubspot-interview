# interview

Hubspot interview code.

Task description found [here](https://tools.hubteam.com/interviewProject/start?key=9622d578a1041e35e0ac0d0e41a6).

## Installation

Install Leiningen & Clojure & Java

Probably `brew install leiningen` will suffice.

```
git clone https://brian_stearns@bitbucket.org/brian_stearns/hubspot-interview.git
cd hubspot-interview
lein deps
```

## Usage

```
cd hubspot-interview
lein repl
;; jvm starts up....
;; .......
interview.core=> (def result (get-process-send))
interview.core=> (:status result) ;; => 200
```
