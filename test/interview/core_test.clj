(ns interview.core-test
  (:require [clojure.test :refer :all]
            [interview.core :refer :all]))

(deftest test-make-url
  (let [endpoint "/my_endpoint"]
    (testing "Makes a URL"
      (is (= "http://example.com/my_endpoint"
             (make-url "http://example.com" endpoint))))
    (testing "No endpoint just hits base-url"
      (is (= "http://example.com"
             (make-url "http://example.com" ""))))))
