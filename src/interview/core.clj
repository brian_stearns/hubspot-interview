(ns interview.core
  (:gen-class)
  (:require [clojure.string :as str :refer [join]]
            [clojure.data.json :as json]
            [clj-time.core :as t]
            [clj-time.format :as f]
            [clojure.string :as str]
            [clj-http.client :as client]))


;; REPL Helper

(defn reload [] (use 'interview.core :reload))


;; API Functions

(def home "https://candidate.hubapi.com/candidateTest/v1")

(def partners-path "/partners")

(def api-key "9622d578a1041e35e0ac0d0e41a6")

(defn make-params
  [param-map]
  "Takes a map of params and composes them with defaults"
  (merge {:userKey api-key} param-map))

(defn make-url
  [api endpoint]
  (str api endpoint))

(defn get-partners
  []
  (let [resp (client/get (make-url home partners-path)
                         {:query-params (make-params {})
                          :as :auto})]
    (json/read-str (:body resp) :key-fn keyword)))

;; DATE HANDLING

(defn read-date
  [date-string]
  "Convert the ISO string to datetime obj"
  (let [[yr mo dy] (map #(Integer/parseInt %) (str/split date-string #"-"))]
    (t/date-time yr mo dy)))

(defn are-sequential-days
  [[d1 d2]]
  "Takes two date-times and tests if the interval is 1, which indicates that they
   are sequential"
  (= 1 (t/in-days (t/interval d1 d2))))

(defn sequential-free-days
  [dates]
  "Assumes the dates are ordered, which is unspecified but appears to be the case.
  Takes a vector of ISO format date strings ('1989-04-15') and returns a vector of
  date-times"
  (filterv are-sequential-days (partition 2 1 (map read-date dates))))

(defn add-free-dates
  [partner]
  "Takes a partner map and returns it with :availableDates interpreted as dates"
  (update partner :availableDates sequential-free-days))

(defn date-to-string
  [date-time]
  (f/unparse (f/formatters :year-month-day) date-time))

;; GROUPING AND SORTING

(defn availabilities
  [partner]
  "Takes a partner map and returns a map with only :dates, :email, and :country
   These are the only fields needed for building the country events list"
  (mapv (fn
          [date]
          {:dates date :email (:email partner) :country (:country partner)})
        (:availableDates (add-free-dates partner))))

(defn individual-availabilities
  [partners]
  "Creates a record for each person in a vector of partners per sequentially 
   available dates"
  (mapcat availabilities partners))

(defn avails-by-cntry
  [partners]
  "Creates a map of countries to individual-availabilities in that country.
   This map looks like:
     {'Japan' {(datetimeA, datetimeB)
                [{:dates (datetimeA, datetimeB)
                  :email 'foo@bar.com'
                  :country 'Japan'},
                 {:dates (datetimeA, datetimeB)
                  :email 'bar@foo.com'
                  :country 'Japan'},]
        .....etc
  "
  (group-by :country (individual-availabilities partners)))

(defn country-event-profile
  [country [dates event-map]]
  (let [attendees (map :email event-map)
        attendee-count (count attendees)
        start-date (first dates)
        name country]
    {:attendees attendees
     :attendeeCount attendee-count
     :name country
     :startDate start-date}))

(def event-sorter
  (juxt :attendeeCount
        :startDate))

(def desc-comp
  "a reverse comparator"
  #(compare %2 %1))


(defn country-shared-dates
  [country-availabilities]
  (group-by :dates country-availabilities))


;; Higher level functions


(defn pick-best-event-profile
  [event-profiles]
  "Takes a list of event-profiles and selects the best one.
   This also preps the date for conversion to JSON, which feels a bit out
   of place but it no longer needs to be a comparator value"
  (let [best (first (sort-by event-sorter desc-comp event-profiles))]
    (update best :startDate date-to-string)))


(defn process-partners
  [partners]
  (let [avails-by-country (avails-by-cntry partners)
        countries (keys avails-by-country)]
    {:countries
     (pmap (fn [c]
            (let [get-country-profiles (partial country-event-profile c) 
                  resident-partners-by-date (country-shared-dates (get avails-by-country c))
                  event-profiles (map get-country-profiles resident-partners-by-date)]
              (pick-best-event-profile event-profiles)))
          countries)}))

;; Run this one

(defn get-process-send
  []
  (let [partners (:partners (get-partners))
        results (process-partners partners)]
    ;; Something is breaking wrt building the URL for post...
    (client/post "https://candidate.hubapi.com/candidateTest/v1/results?userKey=9622d578a1041e35e0ac0d0e41a6"
                 {:body (json/write-str results)})))
  
